<?php


namespace App\Containers\User\Actions;


use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

class CreateUserAction extends Action
{

  public function run(DataTransporter $data)
  {

    $user = Apiato::call('User@CreateUserByCredentialsTask', [
      $isClient = true,
      $data->email,
      $data->password,
      $data->name,
      $data->gender,
      $data->birth
    ]);

    return $user;
  }

}
