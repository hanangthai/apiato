<?php


namespace App\Containers\User\Actions;

use Apiato\Core\Foundation\Facades\Apiato;

class GetUserInstanceAction
{

  public function run()
  {
    $user = Apiato::call('User@GetUserInstanceTask');

    return $user;
  }

}
