<?php


namespace App\Containers\User\UI\WEB\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\User\UI\WEB\Requests\UserFormRequest;
use App\Ship\Parents\Controllers\WebController;
use App\Ship\Transporters\DataTransporter;

class UserController extends WebController
{

  public function getListUsers()
  {
    $users = Apiato::call('User@GetAllUsersAction');
    return view('user::admin.page.index', compact(['users', $users]));
  }

  public function createUserForm()
  {
    $user = Apiato::call('User@GetUserInstanceAction');
    return view('user::admin.page.create', compact(['user', $user]));
  }

  public function createUserPost(UserFormRequest $request)
  {
    Apiato::call('User@CreateUserAction', [new DataTransporter($request)]);
    return redirect()->route('admin_get_list_users')->with('message', __('user::admin/message.create'));
  }

  public function editUserForm($id)
  {
    $user = Apiato::call('User@FindUserByIdAction', [new DataTransporter(['id' => $id])]);
    return view('user::admin.page.edit', compact(['user', $user]));
  }

  public function editUserPost(UserFormRequest $request)
  {
    Apiato::call('User@UpdateUserAction', [new DataTransporter($request)]);
    return redirect()->back()->with('message', __('user::admin/message.update'));
  }

  public function deleteUser($id)
  {
    Apiato::call('User@DeleteUserAction', [new DataTransporter(['id' => $id])]);
    return redirect()->route('admin_get_list_users')->with('message', __('user::admin/message.delete'));
  }
}
