@php
  $breadcrumb = [
      'module' => 'User',
      'root' => 'Home',
      'action' => 'edit'
  ];
  $is_update = true;
  $disable   = 'disabled';
  $readOnly  = 'readonly';
  $required  = '';
  $action_form = route('admin_edit_user_post');
  $button = 'Update';
@endphp
@extends('theme::admin.layout.base')
@section('content')
  <div class="content-page">
    <div class="content"><!-- Start content -->
      <div class="container-fluid">
        @include('theme::admin.helper.breadcrumb')
        <div class="row">
          <div class="col-12">
            <div class="card-box">
              <div class="row justify-content-md-center">
                <div class="col-8">
                  <div class="p-20">
                    @include('user::admin.page.form', ['user' => $user])
                  </div>
                </div>
              </div><!-- end row -->
            </div> <!-- end card-box -->
          </div><!-- end col -->
        </div><!-- end row -->
      </div> <!-- container -->
    </div> <!-- content -->
    @include('theme::admin.block.footer')
  </div>
@endsection
