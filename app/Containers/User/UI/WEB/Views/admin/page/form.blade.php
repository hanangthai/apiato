<form class="form-horizontal" role="form" method="post" action="{{$action_form}}">
  @csrf

  @if($is_update)
    <input type="hidden" name="id" value="{{$user->id}}" required>
  @endif

  <div class="form-group row">
    <label class="col-2 col-form-label">Username</label>
    <div class="col-10">
      <input type="text" {{$disable}}  {{$readOnly}} value="{{$user->name}}" class="form-control" name="name" placeholder="name">
      @include('theme::admin.helper.message.field_message', ['error_name' => 'name'])
    </div>
  </div>

  <div class="form-group row">
    <label class="col-2 col-form-label" for="email">Email</label>
    <div class="col-10">
      <input type="email" {{$disable}}  {{$readOnly}} value="{{$user->email}}" name="email" class="form-control"
             placeholder="email" >
      @include('theme::admin.helper.message.field_message', ['error_name' => 'email'])
    </div>
  </div>

  <div class="form-group row">
    <label class="col-2 col-form-label">DOB</label>
    <div class="col-10">
      <input class="form-control" type="date" name="birth" value="{{$user->birth}}">
      @include('theme::admin.helper.message.field_message', ['error_name' => 'birth'])
    </div>
  </div>

  <div class="form-group row">
    <label class="col-2 col-form-label">Password</label>
    <div class="col-10">
      <input type="password" class="form-control" name="password">
      @include('theme::admin.helper.message.field_message', ['error_name' => 'password'])
    </div>
  </div>

  <div class="form-group row">
    <label class="col-2 col-form-label">Confirmed Password</label>
    <div class="col-10">
      <input type="password" class="form-control" name="password_confirmation">
      @include('theme::admin.helper.message.field_message', ['error_name' => 'password_confirmation'])
    </div>
  </div>

  <div class="form-group row">
    <label class="col-2 col-form-label">Gender</label>
    <div class="col-10">
      <select class="form-control" name="gender">
        <option value="1">Male</option>
        <option value="0">Female</option>
      </select>
      @include('theme::admin.helper.message.field_message', ['error_name' => 'gender'])
    </div>
  </div>

  <div class="form-group text-right m-b-0">
    <button class="btn btn-primary waves-effect waves-light" type="submit">
      {{$button}}
    </button>
    <button type="reset" class="btn btn-secondary waves-effect m-l-5">
      Cancel
    </button>
  </div>

</form>
