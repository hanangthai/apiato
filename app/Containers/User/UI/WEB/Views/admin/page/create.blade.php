@php
  $breadcrumb = [
      'module' => 'User',
      'root' => 'Home',
      'action' => 'create'
  ];
  $is_update = false;
  $disable   = '';
  $readOnly  = '';
  $required  = '';
  $action_form = route('admin_create_user_post');
  $button = 'Create';
@endphp
@extends('theme::admin.layout.base')
@section('content')
  <div class="content-page">
    <div class="content"><!-- Start content -->
      <div class="container-fluid">
        @include('theme::admin.helper.breadcrumb')
        <div class="row">
          <div class="col-12">
            <div class="card-box">
              <div class="row justify-content-md-center">
                <div class="col-8">
                  <div class="p-20">
                    @include('user::admin.page.form')
                  </div>
                </div>
              </div><!-- end row -->
            </div> <!-- end card-box -->
          </div><!-- end col -->
        </div><!-- end row -->
      </div> <!-- container -->
    </div> <!-- content -->
    @include('theme::admin.block.footer')
  </div>
@endsection
