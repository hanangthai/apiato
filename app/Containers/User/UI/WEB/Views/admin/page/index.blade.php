@php
  $breadcrumb = [
      'module' => 'User',
      'root' => 'Home',
      'action' => 'listing'
  ];
@endphp
@extends('theme::admin.layout.base')
@section('content')
  <div class="content-page">
    <div class="content"><!-- Start content -->
      <div class="container-fluid">
        @include('theme::admin.helper.breadcrumb')
        <div class="row">
          <div class="col-sm-12">
            <div class="card-box">

              <div class="btn-group pull-right m-b-20">
                <a href="{{route('admin_create_user_form')}}" class="btn btn-default  waves-effect waves-light" aria-expanded="false">Create</a>
              </div>

              <table class="table">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Gender</th>
                  <th>Dob</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                  <tr>
                    <th scope="row">{{$loop->index}}</th>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->gender_text}}</td>
                    <td>{{$user->birth}}</td>
                    <td>
                      <a href="{{route('admin_edit_user_form', ['id' => $user->id])}}">
                        <i class="dripicons-document-edit"></i>
                      </a>
                      {{'|'}}
                      <a onclick="return confirm('Are you sure you want to delete this item?')" href="{{route('admin_delete_user',['id' => $user->id])}}">
                        <i class="dripicons-trash"></i>
                      </a>
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div> <!-- container -->
    </div> <!-- content -->
    @include('theme::admin.block.footer')
  </div>
@endsection
