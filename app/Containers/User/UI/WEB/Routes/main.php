<?php

$router->get('/user', [
  'as'   => 'admin_get_list_users',
  'uses' => 'UserController@getListUsers',
]);

$router->get('/user/create', [
  'as'   => 'admin_create_user_form',
  'uses' => 'UserController@createUserForm',
]);

$router->post('/user/create', [
  'as'   => 'admin_create_user_post',
  'uses' => 'UserController@createUserPost',
]);

$router->get('/user/edit/{id}', [
  'as'   => 'admin_edit_user_form',
  'uses' => 'UserController@editUserForm',
]);

$router->post('/user/edit', [
  'as'   => 'admin_edit_user_post',
  'uses' => 'UserController@editUserPost',
]);

$router->get('/user/delete/{id}', [
  'as'   => 'admin_delete_user',
  'uses' => 'UserController@deleteUser',
]);
