<?php


namespace App\Containers\User\UI\WEB\Requests;


use App\Ship\Parents\Requests\Request;

class UserFormRequest extends Request
{

  protected $rules = [
    'birth' => 'required|date',
    'gender' => 'numeric',
  ];


  /**
   * Define which Roles and/or Permissions has access to this request.
   *
   * @var  array
   */
  protected $access = [
    'permissions' => null
  ];

  /**
   * Id's that needs decoding before applying the validation rules.
   *
   * @var  array
   */
  protected $decode = [

  ];

  /**
   * Defining the URL parameters (`/stores/999/items`) allows applying
   * validation rules on them and allows accessing them like request data.
   *
   * @var  array
   */
  protected $urlParameters = [

  ];

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return $this->getRules();

  }

  public function getRules()
  {
    return ($this->has('id')) ? $this->updateValidator() : $this->createValidator();
  }

  public function updateValidator()
  {
    $password = ($this->has('password') && ($this->input('password') !== null)) ? ['password' => 'confirmed|min:5|max:30'] : [];
    $email = ($this->has('email')) ? ['email' => 'required|email|unique:users|max:40'] : [];
    $this->rules = array_merge($this->rules, $password, $email);
    return $this->rules;
  }

  public function createValidator()
  {
    $password = ['password' => 'required|confirmed|min:5|max:30'];
    $email    = ['email' => 'required|email|unique:users|max:50'];
    $username = ['name' => 'required|unique:users|min:5|max:50'];

    $this->rules = array_merge($this->rules, $password, $email, $username);
    return $this->rules;
  }

  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return $this->check([
      'hasAccess',
    ]);
  }

}
