<?php

namespace App\Containers\Messages\Tasks;

use App\Containers\Messages\Data\Repositories\MessagesRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteMessagesTask extends Task
{

    protected $repository;

    public function __construct(MessagesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
