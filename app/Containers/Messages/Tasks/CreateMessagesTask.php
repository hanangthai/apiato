<?php

namespace App\Containers\Messages\Tasks;

use App\Containers\Messages\Data\Repositories\MessagesRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateMessagesTask extends Task
{

    protected $repository;

    public function __construct(MessagesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
