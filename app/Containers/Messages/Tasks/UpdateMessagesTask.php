<?php

namespace App\Containers\Messages\Tasks;

use App\Containers\Messages\Data\Repositories\MessagesRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateMessagesTask extends Task
{

    protected $repository;

    public function __construct(MessagesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
