<?php


namespace App\Containers\Messages\Tasks;


use App\Containers\Messages\Data\Repositories\MessagesRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindMessagesByCodeTypeTask extends Task
{
  protected $repository;

  public function __construct(MessagesRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run($code, $type)
  {
    try {
      $message = $this->repository->findWhere(['code' => $code, 'type' => $type]);
      return $message;
    } catch (Exception $exception) {
      throw new NotFoundException();
    }
  }
}
