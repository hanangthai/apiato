<?php

namespace App\Containers\Messages\Tasks;

use App\Containers\Messages\Data\Repositories\MessagesRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindMessagesByIdTask extends Task
{

    protected $repository;

    public function __construct(MessagesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
