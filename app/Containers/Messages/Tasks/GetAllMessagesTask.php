<?php

namespace App\Containers\Messages\Tasks;

use App\Containers\Messages\Data\Repositories\MessagesRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllMessagesTask extends Task
{

    protected $repository;

    public function __construct(MessagesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
