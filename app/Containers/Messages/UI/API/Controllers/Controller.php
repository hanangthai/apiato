<?php

namespace App\Containers\Messages\UI\API\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Messages\UI\API\Requests\CreateMessagesRequest;
use App\Containers\Messages\UI\API\Requests\DeleteMessagesRequest;
use App\Containers\Messages\UI\API\Requests\FindMessagesByCodeTypeRequest;
use App\Containers\Messages\UI\API\Requests\FindMessagesByIdRequest;
use App\Containers\Messages\UI\API\Requests\GetAllMessagesRequest;
use App\Containers\Messages\UI\API\Requests\UpdateMessagesRequest;
use App\Containers\Messages\UI\API\Transformers\MessagesSearchTransformer;
use App\Containers\Messages\UI\API\Transformers\MessagesTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;

/**
 * Class Controller
 *
 * @package App\Containers\Messages\UI\API\Controllers
 */
class Controller extends ApiController
{
  /**
   * @param  CreateMessagesRequest  $request
   * @return JsonResponse
   */
  public function createMessages(CreateMessagesRequest $request)
  {
    $messages = Apiato::call('Messages@CreateMessagesAction', [$request]);

    return $this->created($this->transform($messages, MessagesTransformer::class));
  }

  /**
   * @param  FindMessagesByIdRequest  $request
   * @return array
   */
  public function findMessagesById(FindMessagesByIdRequest $request)
  {
    $messages = Apiato::call('Messages@FindMessagesByIdAction', [$request]);

    return $this->transform($messages, MessagesSearchTransformer::class);
  }

  /**
   * @param  FindMessagesByCodeTypeRequest  $request
   * @return array
   */
  public function findMessageByCodeType(FindMessagesByCodeTypeRequest $request)
  {
    $messages = Apiato::call('Messages@FindMessagesByCodeTypeAction', [$request]);

    return $this->transform($messages, MessagesSearchTransformer::class);
  }

  /**
   * @param  GetAllMessagesRequest  $request
   * @return array
   */
  public function getAllMessages(GetAllMessagesRequest $request)
  {
    $messages = Apiato::call('Messages@GetAllMessagesAction', [$request]);

    return $this->transform($messages, MessagesTransformer::class);
  }

  /**
   * @param  UpdateMessagesRequest  $request
   * @return array
   */
  public function updateMessages(UpdateMessagesRequest $request)
  {
    $messages = Apiato::call('Messages@UpdateMessagesAction', [$request]);

    return $this->transform($messages, MessagesTransformer::class);
  }

  /**
   * @param  DeleteMessagesRequest  $request
   * @return JsonResponse
   */
  public function deleteMessages(DeleteMessagesRequest $request)
  {
    Apiato::call('Messages@DeleteMessagesAction', [$request]);

    return $this->noContent();
  }
}
