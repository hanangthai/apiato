<?php

namespace App\Containers\Messages\UI\API\Transformers;

use App\Containers\Messages\Models\Messages;
use App\Ship\Parents\Transformers\Transformer;

class MessagesTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Messages $entity
     *
     * @return array
     */
    public function transform(Messages $entity)
    {
        $response = [
            'object' => 'Messages',
            'id' => $entity->getHashedKey(),
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
