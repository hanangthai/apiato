<?php

/**
 * @apiGroup           Messages
 * @apiName            Controller
 *
 * @api                {GET} /v1/message/:code/:type Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('message/{code}/{type}', [
    'as' => 'api_find_messages_by_code_type',
    'uses'  => 'Controller@findMessageByCodeType',
    'middleware' => [
      'auth:api',
    ],
]);
