<?php

/**
 * @apiGroup           Messages
 * @apiName            deleteMessages
 *
 * @api                {DELETE} /v1/message/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('message/{id}', [
    'as' => 'api_messages_delete_messages',
    'uses'  => 'Controller@deleteMessages',
    'middleware' => [
      'auth:api',
    ],
]);
