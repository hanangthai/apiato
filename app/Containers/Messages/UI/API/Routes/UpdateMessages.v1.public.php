<?php

/**
 * @apiGroup           Messages
 * @apiName            updateMessages
 *
 * @api                {PATCH} /v1/message/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('message/{id}', [
    'as' => 'api_messages_update_messages',
    'uses'  => 'Controller@updateMessages',
    'middleware' => [
      'auth:api',
    ],
]);
