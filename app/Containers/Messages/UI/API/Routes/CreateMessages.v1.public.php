<?php

/**
 * @apiGroup           Messages
 * @apiName            createMessages
 *
 * @api                {POST} /v1/message Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('message', [
    'as' => 'api_messages_create_messages',
    'uses'  => 'Controller@createMessages',
    'middleware' => [
      'auth:api',
    ],
]);
