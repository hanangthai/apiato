@php
  $breadcrumb = [
      'module' => 'User',
      'root' => 'Home',
      'action' => 'listing'
  ];
@endphp
@extends('theme::admin.layout.base')
@section('content')
  <div class="content-page">
    <div class="content"><!-- Start content -->
      <div class="container-fluid">
        @include('theme::admin.helper.breadcrumb')
        <div class="row">
          <div class="col-sm-12">
            <div class="card-box">

              <div class="btn-group pull-right m-b-20">
                <a href="#" class="btn btn-default  waves-effect waves-light" aria-expanded="false">Create</a>
              </div>

              <table class="table">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Code</th>
                  <th>Content</th>
                  <th>Type</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($messages as $message)
                  <tr>
                    <th scope="row">{{$loop->index}}</th>
                    <td>{{$message->code}}</td>
                    <td>{{$message->content}}</td>
                    <td>{{$message->type}}</td>
                    <td>
                      <a href="#">
                        <i class="dripicons-document-edit"></i>
                      </a>
                      {{'|'}}
                      <a  href="#">
                        <i class="dripicons-trash"></i>
                      </a>
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div> <!-- container -->
    </div> <!-- content -->
    @include('theme::admin.block.footer')
  </div>
@endsection
