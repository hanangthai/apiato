<?php


namespace App\Containers\Messages\UI\WEB\Controllers;


use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Messages\UI\WEB\Requests\CreateMessagesRequest;
use App\Containers\Messages\UI\WEB\Requests\DeleteMessagesRequest;
use App\Containers\Messages\UI\WEB\Requests\EditMessagesRequest;
use App\Containers\Messages\UI\WEB\Requests\FindMessagesByIdRequest;
use App\Containers\Messages\UI\WEB\Requests\GetAllMessagesRequest;
use App\Containers\Messages\UI\WEB\Requests\StoreMessagesRequest;
use App\Containers\Messages\UI\WEB\Requests\UpdateMessagesRequest;
use App\Ship\Parents\Controllers\WebController;

class MessageController extends WebController
{

  public function getListMessages(GetAllMessagesRequest $request)
  {
    $messages = Apiato::call('Messages@GetAllMessagesAction', [$request]);
    return view('messages::admin.page.index', compact(['messages', $messages]));
  }

  public function createMessageForm(FindMessagesByIdRequest $request)
  {
    $messages = Apiato::call('Messages@FindMessagesByIdAction', [$request]);

    // ..
  }

  public function create(CreateMessagesRequest $request)
  {
    // ..
  }

  public function store(StoreMessagesRequest $request)
  {
    $messages = Apiato::call('Messages@CreateMessagesAction', [$request]);

    // ..
  }

  public function edit(EditMessagesRequest $request)
  {
    $messages = Apiato::call('Messages@GetMessagesByIdAction', [$request]);

    // ..
  }

  public function update(UpdateMessagesRequest $request)
  {
    $messages = Apiato::call('Messages@UpdateMessagesAction', [$request]);

    // ..
  }

  public function delete(DeleteMessagesRequest $request)
  {
    $result = Apiato::call('Messages@DeleteMessagesAction', [$request]);

    // ..
  }

}
