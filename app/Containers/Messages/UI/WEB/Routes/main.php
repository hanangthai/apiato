<?php

$router->get('/message', [
  'as'   => 'admin_get_list_messages',
  'uses' => 'MessageController@getListMessages',
]);

$router->get('/message/create', [
  'as'   => 'admin_create_message_form',
  'uses' => 'MessageController@createMessageForm',
]);

$router->post('/message/create', [
  'as'   => 'admin_create_message_post',
  'uses' => 'MessageController@createMessagePost',
]);

$router->get('/message/edit/{id}', [
  'as'   => 'admin_edit_message_form',
  'uses' => 'MessageController@editMessageForm',
]);

$router->post('/message/edit', [
  'as'   => 'admin_edit_message_post',
  'uses' => 'MessageController@editMessagePost',
]);

$router->get('/message/delete/{id}', [
  'as'   => 'admin_delete_message',
  'uses' => 'MessageController@deleteMessage',
]);
