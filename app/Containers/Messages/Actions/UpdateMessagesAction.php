<?php

namespace App\Containers\Messages\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateMessagesAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $messages = Apiato::call('Messages@UpdateMessagesTask', [$request->id, $data]);

        return $messages;
    }
}
