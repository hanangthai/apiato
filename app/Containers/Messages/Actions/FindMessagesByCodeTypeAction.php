<?php


namespace App\Containers\Messages\Actions;


use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindMessagesByCodeTypeAction extends Action
{
  public function run(Request $request)
  {

    $message = Apiato::call('Messages@FindMessagesByCodeTypeTask', [$request->code, $request->type]);

    return $message;
  }

}
