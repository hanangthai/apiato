<?php

namespace App\Containers\Messages\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindMessagesByIdAction extends Action
{
    public function run(Request $request)
    {
        $messages = Apiato::call('Messages@FindMessagesByIdTask', [$request->id]);

        return $messages;
    }
}
