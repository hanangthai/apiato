<?php

namespace App\Containers\Messages\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteMessagesAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Messages@DeleteMessagesTask', [$request->id]);
    }
}
