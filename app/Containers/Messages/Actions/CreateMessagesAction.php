<?php

namespace App\Containers\Messages\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Messages\UI\API\Requests\CreateMessagesRequest;
use App\Ship\Parents\Actions\Action;

class CreateMessagesAction extends Action
{
    public function run(CreateMessagesRequest $request)
    {
        $data = [
          'code' => $request->input('code'),
          'type' => $request->input('type'),
          'content' => $request->input('content')
        ];

        $messages = Apiato::call('Messages@CreateMessagesTask', [$data]);

        return $messages;
    }
}
