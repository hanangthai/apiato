<?php

namespace App\Containers\Messages\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class MessagesRepository
 */
class MessagesRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
    ];

}
