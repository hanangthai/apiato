<?php

namespace App\Containers\Messages\Models;

use App\Ship\Parents\Models\Model;

class Messages extends Model
{
    protected $fillable = [
      'code', 'type', 'content'
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'messages';
}
