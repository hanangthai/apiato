<?php

namespace App\Containers\Theme\Tasks;

use App\Containers\Theme\Data\Repositories\ThemeRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateThemeTask extends Task
{

    protected $repository;

    public function __construct(ThemeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
