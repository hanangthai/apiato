<?php

namespace App\Containers\Theme\Tasks;

use App\Containers\Theme\Data\Repositories\ThemeRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllThemesTask extends Task
{

    protected $repository;

    public function __construct(ThemeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
