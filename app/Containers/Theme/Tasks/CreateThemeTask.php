<?php

namespace App\Containers\Theme\Tasks;

use App\Containers\Theme\Data\Repositories\ThemeRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateThemeTask extends Task
{

    protected $repository;

    public function __construct(ThemeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
