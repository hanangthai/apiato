<?php

namespace App\Containers\Theme\Tasks;

use App\Containers\Theme\Data\Repositories\ThemeRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindThemeByIdTask extends Task
{

    protected $repository;

    public function __construct(ThemeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
