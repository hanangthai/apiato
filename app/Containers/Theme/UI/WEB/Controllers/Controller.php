<?php

namespace App\Containers\Theme\UI\WEB\Controllers;

use App\Containers\Theme\UI\WEB\Requests\CreateThemeRequest;
use App\Containers\Theme\UI\WEB\Requests\DeleteThemeRequest;
use App\Containers\Theme\UI\WEB\Requests\GetAllThemesRequest;
use App\Containers\Theme\UI\WEB\Requests\FindThemeByIdRequest;
use App\Containers\Theme\UI\WEB\Requests\UpdateThemeRequest;
use App\Containers\Theme\UI\WEB\Requests\StoreThemeRequest;
use App\Containers\Theme\UI\WEB\Requests\EditThemeRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Theme\UI\WEB\Controllers
 */
class Controller extends WebController
{
    /**
     * Show all entities
     *
     * @param GetAllThemesRequest $request
     */
    public function index(GetAllThemesRequest $request)
    {
        $themes = Apiato::call('Theme@GetAllThemesAction', [$request]);

        // ..
    }

    /**
     * Show one entity
     *
     * @param FindThemeByIdRequest $request
     */
    public function show(FindThemeByIdRequest $request)
    {
        $theme = Apiato::call('Theme@FindThemeByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateThemeRequest $request
     */
    public function create(CreateThemeRequest $request)
    {
        // ..
    }

    /**
     * Add a new entity
     *
     * @param StoreThemeRequest $request
     */
    public function store(StoreThemeRequest $request)
    {
        $theme = Apiato::call('Theme@CreateThemeAction', [$request]);

        // ..
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditThemeRequest $request
     */
    public function edit(EditThemeRequest $request)
    {
        $theme = Apiato::call('Theme@GetThemeByIdAction', [$request]);

        // ..
    }

    /**
     * Update a given entity
     *
     * @param UpdateThemeRequest $request
     */
    public function update(UpdateThemeRequest $request)
    {
        $theme = Apiato::call('Theme@UpdateThemeAction', [$request]);

        // ..
    }

    /**
     * Delete a given entity
     *
     * @param DeleteThemeRequest $request
     */
    public function delete(DeleteThemeRequest $request)
    {
         $result = Apiato::call('Theme@DeleteThemeAction', [$request]);

         // ..
    }
}
