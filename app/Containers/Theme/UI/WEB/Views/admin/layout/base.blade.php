<!DOCTYPE html>
<html>
<head>
  @include('theme::admin.block.head')
</head>
<body class="fixed-left">

<div id="wrapper">
  @include('theme::admin.block.topbar')
  @include('theme::admin.block.sidebar')
  @yield('content')
</div>

<script>
  var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{asset('admin')}}/assets/js/jquery.min.js"></script>
<script src="{{asset('admin')}}/assets/js/popper.min.js"></script>
<script src="{{asset('admin')}}/assets/js/bootstrap.min.js"></script>
<script src="{{asset('admin')}}/assets/js/detect.js"></script>
<script src="{{asset('admin')}}/assets/js/fastclick.js"></script>
<script src="{{asset('admin')}}/assets/js/jquery.slimscroll.js"></script>
<script src="{{asset('admin')}}/assets/js/jquery.blockUI.js"></script>
<script src="{{asset('admin')}}/assets/js/waves.js"></script>
<script src="{{asset('admin')}}/assets/js/wow.min.js"></script>
<script src="{{asset('admin')}}/assets/js/jquery.nicescroll.js"></script>
<script src="{{asset('admin')}}/assets/js/jquery.scrollTo.min.js"></script>
<script src="{{asset('admin')}}/assets/plugin/bootstrap-table/js/bootstrap-table.js"></script>
<script src="{{asset('admin')}}/assets/pages/jquery.bs-table.js"></script>
<script src="{{asset('admin')}}/assets/plugin/notifyjs/js/notify.js"></script>
<script src="{{asset('admin')}}/assets/plugin/notifications/notify-metro.js"></script>
<script src="{{asset('admin')}}/assets/js/jquery.core.js"></script>
<script src="{{asset('admin')}}/assets/js/jquery.app.js"></script>

@if (session('message'))
  <script>
    $.Notification.notify('success','bottom right','Notification', '{{ session('message') }}')
  </script>
@endif

</body>
</html>
