<div class="left side-menu">
  <div class="sidebar-inner slimscrollleft">
    <div id="sidebar-menu">
      <ul>
        <li class="text-muted menu-title">Admin menu</li>
        <li class="has_sub">
          <a href="javascript:void(0);" class="waves-effect"><i class="ti-user"></i> <span> Users </span>
            <span class="menu-arrow"></span></a>
          <ul class="list-unstyled">
            <li><a href="{{route('admin_get_list_users')}}">users</a></li>
          </ul>
        </li>

        <li class="has_sub">
          <a href="javascript:void(0);" class="waves-effect"><i class="ti-themify-favicon-alt"></i> <span> Messages </span>
            <span class="menu-arrow"></span></a>
          <ul class="list-unstyled">
            <li><a href="{{route('admin_get_list_messages')}}">messages</a></li>
          </ul>
        </li>

      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
