
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
<meta name="author" content="Coderthemes">

<link rel="shortcut icon" href="{{asset('admin')}}/assets/images/favicon.ico">

<title>Apiato Content Management System</title>

<link href="{{asset('admin')}}/assets/plugin/bootstrap-table/css/bootstrap-table.min.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin')}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin')}}/assets/css/icons.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin')}}/assets/css/style.css" rel="stylesheet" type="text/css"/>
<script src="{{asset('admin')}}/assets/js/modernizr.min.js"></script>
