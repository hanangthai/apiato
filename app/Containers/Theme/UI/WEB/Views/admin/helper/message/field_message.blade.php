@if ($errors->has($error_name))
    <span class="help-block" style="color: red; font-size: 10px;">
       <strong>{{ $errors->first($error_name) }}</strong>
    </span>
@endif
