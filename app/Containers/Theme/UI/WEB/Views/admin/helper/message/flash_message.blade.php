@if (session('message'))
    <div class="row box-body">
        <div class="col-xs-12">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ session('message') }}
            </div>
        </div>
    </div>
@endif
