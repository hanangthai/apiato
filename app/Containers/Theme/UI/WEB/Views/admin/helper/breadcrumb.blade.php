<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Module {{$breadcrumb['module']}}</h4>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">{{$breadcrumb['root']}}</a></li>
      <li class="breadcrumb-item"><a href="#">{{$breadcrumb['module']}}</a></li>
      <li class="breadcrumb-item active"><a href="#">{{$breadcrumb['action']}}</a></li>
    </ol>
  </div>
</div>
