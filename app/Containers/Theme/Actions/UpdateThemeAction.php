<?php

namespace App\Containers\Theme\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateThemeAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $theme = Apiato::call('Theme@UpdateThemeTask', [$request->id, $data]);

        return $theme;
    }
}
