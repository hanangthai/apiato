<?php

namespace App\Containers\Theme\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteThemeAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Theme@DeleteThemeTask', [$request->id]);
    }
}
