<?php

namespace App\Containers\Theme\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllThemesAction extends Action
{
    public function run(Request $request)
    {
        $themes = Apiato::call('Theme@GetAllThemesTask', [], ['addRequestCriteria']);

        return $themes;
    }
}
