<?php

namespace App\Containers\Theme\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindThemeByIdAction extends Action
{
    public function run(Request $request)
    {
        $theme = Apiato::call('Theme@FindThemeByIdTask', [$request->id]);

        return $theme;
    }
}
