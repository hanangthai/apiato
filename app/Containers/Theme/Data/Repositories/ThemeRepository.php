<?php

namespace App\Containers\Theme\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ThemeRepository
 */
class ThemeRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
